require('mocha');

const { assert } = require('chai'); // Using Assert style
const { requireBox } = require('@liquidapps/box-utils');

const artifacts = requireBox('seed-eos/tools/eos/artifacts');
const deployer = requireBox('seed-eos/tools/eos/deployer');
const { genAllocateDAPPTokens } = requireBox('dapp-services/tools/eos/dapp-services');
const { getTestContract, getLocalDSPEos } = requireBox('seed-eos/tools/eos/utils');
const { getCreateKeys } = requireBox('eos-keystore/helpers/key-utils');

const { eosio } = requireBox('test-extensions/lib/index');

var contractCode = 'heyleysoutc1';
var ctrt = artifacts.require(`./${contractCode}/`);

describe(`${contractCode} Contract`, () => {
    let testcontract;
    const code = 'heyleysoutc1';
    before(done => {
        (async () => {
            try {
                var deployedContract = await deployer.deploy(ctrt, code);
                await genAllocateDAPPTokens(deployedContract, "oracle", "pprovider1", "default");
                await genAllocateDAPPTokens(deployedContract, "oracle", "pprovider2", "foobar");
                dspeos = await getLocalDSPEos(code);
                testcontract = await getTestContract(code);

                done();
            }
            catch (e) {
                done(e);
            }
        })();
    });

    it('getoutcdet', done => {
        (async () => {
            try {
                testcontract = await getTestContract(code);
                var res = await testcontract.getoutcdet({
                    user: "heyleysoutc1",
                    nctid: "NCT01874691"
                }, {
                    authorization: `${code}@active`,
                    broadcast: true,
                    sign: true
                });
                await eosio.delay(1000);
                res = await dspeos.getTableRows({
                    'json': true,
                    'scope': code,
                    'code': code,
                    'table': 'outcomes',
                    'limit': 2
                });

                assert.ok(res.rows[0].outcome.id == 01874691, 'Id of first entry is not the same as given id.');
                done();
            }
            catch (e) {
                done(e);
            }
        })();
    });

    it('upsertmult', done => {
        (async () => {
            try {
                testcontract = await getTestContract(code);
                var res = await testcontract.upsertmult({
                    user: "heyleysstud1",
                    outcomes: [
                        {
                            id: 123,
                            nctid: "NCT123",
                            primary_outcome_description: "sadfasdf",
                            primary_outcome_measure: "sadfasdf",
                            primary_outcome_timeframe: "sadfasdf",
                            secondary_outcome_description: "sadfasdf",
                            secondary_outcome_measure: "sadfasdf",
                            secondary_outcome_timeframe: "sadfasdf"
                        },
                        {
                            id: 123234,
                            nctid: "NCT123234",
                            primary_outcome_description: "sadfasdf",
                            primary_outcome_measure: "sadfasdf",
                            primary_outcome_timeframe: "sadfasdf",
                            secondary_outcome_description: "sadfasdf",
                            secondary_outcome_measure: "sadfasdf",
                            secondary_outcome_timeframe: "sadfasdf"
                        }
                    ]
                }, {
                    authorization: `${code}@active`,
                    broadcast: true,
                    sign: true
                });
                await eosio.delay(1000);
                res = await dspeos.getTableRows({
                    'json': true,
                    'scope': code,
                    'code': code,
                    'table': 'outcomes',
                    'limit': 2
                });

                assert.ok(res.rows[0].outcome.id == 123, 'Id of first entry is not the same as given id.');
                assert.ok(res.rows[0].outcome.secondary_outcome_description == 'sadfasdf', 'secondaryoutcomedesc of first entry is not the same as given secondaryoutcomedesc.');
                assert.ok(res.rows[1].outcome.id == 123234, 'Id of second entry is not the same as given id.');
                assert.ok(res.rows[1].outcome.secondary_outcome_description == 'sadfasdf', 'secondaryoutcomedesc of second entry is not the same as given secondaryoutcomedesc.');
                done();
            }
            catch (e) {
                done(e);
            }
        })();
    });
});