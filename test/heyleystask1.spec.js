require('mocha');

const { assert } = require('chai'); // Using Assert style
const { requireBox } = require('@liquidapps/box-utils');

const artifacts = requireBox('seed-eos/tools/eos/artifacts');
const deployer = requireBox('seed-eos/tools/eos/deployer');
const { genAllocateDAPPTokens } = requireBox('dapp-services/tools/eos/dapp-services');
const { getTestContract, getLocalDSPEos } = requireBox('seed-eos/tools/eos/utils');
const { getCreateKeys } = requireBox('eos-keystore/helpers/key-utils');

const { eosio } = requireBox('test-extensions/lib/index');

var contractCode1 = 'heyleystask1';
var contractCode2 = 'heyleysstud2';
var contractCode3 = 'heyleyscont1';
var contractCode4 = 'heyleysoutc1';
var ctrt1 = artifacts.require(`./${contractCode1}/`);
var ctrt2 = artifacts.require(`./${contractCode2}/`);
var ctrt3 = artifacts.require(`./${contractCode3}/`);
var ctrt4 = artifacts.require(`./${contractCode4}/`);

describe(`${contractCode1} Contract`, () => {
    let testcontract1;
    let testcontract2;
    let testcontract3;
    let testcontract4;

    before(done => {
        (async () => {
            try {
                var deployedContract1 = await deployer.deploy(ctrt1, contractCode1);
                var deployedContract2 = await deployer.deploy(ctrt2, contractCode2);
                var deployedContract3 = await deployer.deploy(ctrt3, contractCode3);
                var deployedContract4 = await deployer.deploy(ctrt4, contractCode4);

                await deployer.setActionPermissions(deployedContract1.account, deployedContract2.account, deployedContract1.keys, "upsert");
                await deployer.setActionPermissions(deployedContract1.account, deployedContract3.account, deployedContract1.keys, "upsert");
                await deployer.setActionPermissions(deployedContract1.account, deployedContract4.account, deployedContract1.keys, "upsert");

                await genAllocateDAPPTokens(deployedContract1, 'cron');
                await genAllocateDAPPTokens(deployedContract1, "oracle", "pprovider1", "default");
                await genAllocateDAPPTokens(deployedContract1, "oracle", "pprovider2", "foobar");
                await genAllocateDAPPTokens(deployedContract2, "oracle", "pprovider1", "default");
                await genAllocateDAPPTokens(deployedContract2, "oracle", "pprovider2", "foobar");
                await genAllocateDAPPTokens(deployedContract3, "oracle", "pprovider1", "default");
                await genAllocateDAPPTokens(deployedContract3, "oracle", "pprovider2", "foobar");
                await genAllocateDAPPTokens(deployedContract4, "oracle", "pprovider1", "default");
                await genAllocateDAPPTokens(deployedContract4, "oracle", "pprovider2", "foobar");
                dspeos = await getLocalDSPEos(contractCode2);
                testcontract1 = await getTestContract(contractCode1);
                testcontract2 = await getTestContract(contractCode2);
                testcontract3 = await getTestContract(contractCode3);
                testcontract4 = await getTestContract(contractCode4);

                done();
            }
            catch (e) {
                done(e);
            }
        })();
    });

    it('Cron check if ran and results are persisted', done => {
        (async () => {
            try {
                var res = await testcontract1.testharmony({
                }, {
                    authorization: `${contractCode1}@active`,
                    broadcast: true,
                    sign: true
                });

                await eosio.delay(150000);
                res = await dspeos.getTableRows({
                    'json': true,
                    'scope': contractCode2,
                    'code': contractCode2,
                    'table': 'details',
                    'limit': 3
                });
                console.log(res.rows);

                // assert.ok(res.rows.length > 1, 'counter did not increase');

                // res = await dspeos.getTableRows({
                //     'json': true,
                //     'scope': contractCode3,
                //     'code': contractCode3,
                //     'table': 'contacts',
                //     'limit': 3
                // });

                // console.log(res.rows);

                // assert.ok(res.rows.length > 1, 'counter did not increase');

                // res = await dspeos.getTableRows({
                //     'json': true,
                //     'scope': contractCode4,
                //     'code': contractCode4,
                //     'table': 'outcomes',
                //     'limit': 3
                // });

                // console.log(res.rows);

                // assert.ok(res.rows.length > 1, 'counter did not increase');

                // await testcontract1.removetimer({
                //     account: contractCode1
                // }, {
                //     authorization: `${contractCode1}@active`,
                //     broadcast: true,
                //     sign: true
                // });
                done();
            }
            catch (e) {
                done(e);
            }
        })();
    });
});