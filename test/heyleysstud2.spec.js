require('mocha');

const { assert } = require('chai'); // Using Assert style
const { requireBox } = require('@liquidapps/box-utils');

const artifacts = requireBox('seed-eos/tools/eos/artifacts');
const deployer = requireBox('seed-eos/tools/eos/deployer');
const { genAllocateDAPPTokens } = requireBox('dapp-services/tools/eos/dapp-services');
const { getTestContract, getLocalDSPEos } = requireBox('seed-eos/tools/eos/utils');
const { getCreateKeys } = requireBox('eos-keystore/helpers/key-utils');

const { eosio } = requireBox('test-extensions/lib/index');

var contractCode = 'heyleysstud2';
var ctrt = artifacts.require(`./${contractCode}/`);

describe(`${contractCode} Contract`, () => {
    let testcontract;
    const code = 'heyleysstud2';
    before(done => {
        (async () => {
            try {
                var deployedContract = await deployer.deploy(ctrt, code);
                await genAllocateDAPPTokens(deployedContract, "oracle", "pprovider1", "default");
                await genAllocateDAPPTokens(deployedContract, "oracle", "pprovider2", "foobar");
                dspeos = await getLocalDSPEos(code);
                testcontract = await getTestContract(code);

                done();
            }
            catch (e) {
                done(e);
            }
        })();
    });

    it('getstudydet', done => {
        (async () => {
            try {
                testcontract = await getTestContract(code);
                var res = await testcontract.getstudydet({
                    user: "heyleysstud2",
                    nctid: "NCT01874691"
                }, {
                    authorization: `${code}@active`,
                    broadcast: true,
                    sign: true
                });
                await eosio.delay(1000);
                res = await dspeos.getTableRows({
                    'json': true,
                    'scope': code,
                    'code': code,
                    'table': 'details',
                    'limit': 2
                });

                assert.ok(res.rows[0].detail.id == 01874691, 'Id of first entry is not the same as given id.');
                assert.ok(res.rows[0].detail.condition == 'Acute Myocardial Infarction', 'condition of first entry is not the same as given condition.');
                done();
            }
            catch (e) {
                done(e);
            }
        })();
    });

    it('upsertmult', done => {
        (async () => {
            try {
                testcontract = await getTestContract(code);
                var res = await testcontract.upsertmult({
                    user: "heyleysstud2",
                    studies: [
                        {
                            id: 123,
                            nctid: "NCT123",
                            registry_date: "sadfasdf",
                            primary_sponsor: "sadfasdf",
                            secondary_sponsors: "sadfasdf",
                            public_title: "sadfasdf",
                            scientific_title: "sadfasdf",
                            country: "sadfasdf",
                            condition: "sadfasdf",
                            intervention: "sadfasdf",
                            eligibility_criteria: "sadfasdf",
                            study_type: "sadfasdf",
                            start_date: "sadfasdf",
                            last_known_status: "sadfasdf",
                            overall_status: "sadfasdf",
                            completion_date: "sadfasdf",
                            acronym: "sadfasdf",
                            minimum_age: "sadfasdf",
                            maximum_age: "sadfasdf",
                            gender: "sadfasdf",
                            brief_summary: "sadfasdf"
                        },
                        {
                            id: 123234,
                            nctid: "NCT123234",
                            registry_date: "sadfasdf",
                            primary_sponsor: "sadfasdf",
                            secondary_sponsors: "sadfasdf",
                            public_title: "sadfasdf",
                            scientific_title: "sadfasdf",
                            country: "sadfasdf",
                            condition: "sadfasdf",
                            intervention: "sadfasdf",
                            eligibility_criteria: "sadfasdf",
                            study_type: "sadfasdf",
                            start_date: "sadfasdf",
                            last_known_status: "sadfasdf",
                            overall_status: "sadfasdf",
                            completion_date: "sadfasdf",
                            acronym: "sadfasdf",
                            minimum_age: "sadfasdf",
                            maximum_age: "sadfasdf",
                            gender: "sadfasdf",
                            brief_summary: "sadfasdf"
                        }
                    ]
                }, {
                    authorization: `${code}@active`,
                    broadcast: true,
                    sign: true
                });
                await eosio.delay(1000);
                res = await dspeos.getTableRows({
                    'json': true,
                    'scope': code,
                    'code': code,
                    'table': 'details',
                    'limit': 2
                });

                assert.ok(res.rows[0].detail.id == 123, 'Id of first entry is not the same as given id.');
                assert.ok(res.rows[0].detail.country == 'sadfasdf', 'country of first entry is not the same as given country.');
                assert.ok(res.rows[1].detail.id == 123234, 'Id of second entry is not the same as given id.');
                assert.ok(res.rows[1].detail.country == 'sadfasdf', 'country of second entry is not the same as given country.');
                done();
            }
            catch (e) {
                done(e);
            }
        })();
    });
});