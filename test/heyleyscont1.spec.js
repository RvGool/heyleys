require('mocha');

const { assert } = require('chai'); // Using Assert style
const { requireBox } = require('@liquidapps/box-utils');

const artifacts = requireBox('seed-eos/tools/eos/artifacts');
const deployer = requireBox('seed-eos/tools/eos/deployer');
const { genAllocateDAPPTokens } = requireBox('dapp-services/tools/eos/dapp-services');
const { getTestContract, getLocalDSPEos } = requireBox('seed-eos/tools/eos/utils');
const { getCreateKeys } = requireBox('eos-keystore/helpers/key-utils');

const { eosio } = requireBox('test-extensions/lib/index');

var contractCode = 'heyleyscont1';
var ctrt = artifacts.require(`./${contractCode}/`);

describe(`${contractCode} Contract`, () => {
    let testcontract;
    const code = 'heyleyscont1';
    before(done => {
        (async () => {
            try {
                var deployedContract = await deployer.deploy(ctrt, code);
                await genAllocateDAPPTokens(deployedContract, "oracle", "pprovider1", "default");
                await genAllocateDAPPTokens(deployedContract, "oracle", "pprovider2", "foobar");
                dspeos = await getLocalDSPEos(code);
                testcontract = await getTestContract(code);

                done();
            }
            catch (e) {
                done(e);
            }
        })();
    });

    it('getcontdet', done => {
        (async () => {
            try {
                testcontract = await getTestContract(code);
                var res = await testcontract.getcontdet({
                    user: "heyleyscont1",
                    nctid: "NCT01874691"
                }, {
                    authorization: `${code}@active`,
                    broadcast: true,
                    sign: true
                });
                await eosio.delay(1000);
                res = await dspeos.getTableRows({
                    'json': true,
                    'scope': code,
                    'code': code,
                    'table': 'contacts',
                    'limit': 2
                });

                assert.ok(res.rows[0].contact.id == 01874691, 'Id of first entry is not the same as given id.');
                done();
            }
            catch (e) {
                done(e);
            }
        })();
    });
    
    it('upsertmult', done => {
            (async () => {
                try {
                    testcontract = await getTestContract(code);
                    var res = await testcontract.upsertmult({
                        user: "heyleyscont1",
                        contacts: [
                            {
                                id: 123,
                                nctid: "NCT123",
                                central_contact_email: "sadfasdf",
                                central_contact_name: "sadfasdf",
                                central_contact_phone: "sadfasdf",
                                central_contact_phone_ext: "sadfasdf",
                                central_contact_role: "sadfasdf",
                                location_contact_email: "sadfasdf",
                                location_contact_name: "sadfasdf",
                                location_contact_phone: "sadfasdf",
                                location_contact_phone_ext: "sadfasdf",
                                location_contact_role: "sadfasdf",
                                point_of_contact_email: "sadfasdf",
                                point_of_contact_organization: "sadfasdf",
                                point_of_contact_phone: "sadfasdf",
                                point_of_contact_phone_ext: "sadfasdf",
                                point_of_contact_title: "sadfasdf"
                            },
                            {
                                id: 123234,
                                nctid: "NCT123234",
                                central_contact_email: "sadfasdf",
                                central_contact_name: "sadfasdf",
                                central_contact_phone: "sadfasdf",
                                central_contact_phone_ext: "sadfasdf",
                                central_contact_role: "sadfasdf",
                                location_contact_email: "sadfasdf",
                                location_contact_name: "sadfasdf",
                                location_contact_phone: "sadfasdf",
                                location_contact_phone_ext: "sadfasdf",
                                location_contact_role: "sadfasdf",
                                point_of_contact_email: "sadfasdf",
                                point_of_contact_organization: "sadfasdf",
                                point_of_contact_phone: "sadfasdf",
                                point_of_contact_phone_ext: "sadfasdf",
                                point_of_contact_title: "sadfasdf"
                            }
                        ]
                    }, {
                        authorization: `${code}@active`,
                        broadcast: true,
                        sign: true
                    });
                    await eosio.delay(1000);
                    res = await dspeos.getTableRows({
                        'json': true,
                        'scope': code,
                        'code': code,
                        'table': 'contacts',
                        'limit': 2
                    });
    
                    assert.ok(res.rows[0].contact.id == 123, 'Id of first entry is not the same as given id.');
                    assert.ok(res.rows[0].contact.central_contact_email == 'sadfasdf', 'central_contact_email of first entry is not the same as given central_contact_email.');
                    assert.ok(res.rows[1].contact.id == 123234, 'Id of second entry is not the same as given id.');
                    assert.ok(res.rows[1].contact.central_contact_email == 'sadfasdf', 'central_contact_email of second entry is not the same as given central_contact_email.');
                    done();
                }
                catch (e) {
                    done(e);
                }
            })();
    });
});