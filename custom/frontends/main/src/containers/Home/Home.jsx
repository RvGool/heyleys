import React, { Component } from 'react';
import classes from './Home.module.scss';
import Header from 'components/Header/Header';
import StudiesData from 'components/Home/StudiesData/StudiesData';
import Disclaimer from 'components/Home/Disclaimer/Disclaimer';
import Footer from 'components/Footer/Footer';
import ApiService from 'lib/services/ApiService';
import { result } from 'lodash';
const { seedPrivate } = require('eosjs-ecc');
const initialState = {
  // login
  displayLogin: false,
  loginError: 'none',
  isSigningIn: false,
  isLoggedIn: false,
  studiesData: []
};

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = initialState;
  }

  async componentDidMount() {
    this.isComponentMounted = true;
    const studiesData = await ApiService.getStudies();
    this.setState({studiesData: studiesData});
  }

  componentWillUnmount() {
    this.isComponentMounted = false;
  }

  showHideLogin = () => {
    this.setState({ displayLogin: !this.state.displayLogin, addAccountErr: `` });
  }

  render() {
    return (
      <div>
        <div className={classes.rootContainer}>
        <Header
        />
        <div className={classes.container}>
          <div className="stats">
            <h2>{this.state.studiesData.length} Klinische onderzoeken gevonden </h2>
            {
              this.state.studiesData.map((entry) => {
                return (
                  <div key={entry.detail.id} className={classes.studycontainer}>
                    <div className={classes.studyheader}>
                      <h2>{entry.detail.public_title}</h2>
                      <div className={classes.studybtns}>
                        <div className={classes.btn}>
                          <div className={classes.inner}>
                            <div className={classes.label}>
                              Print onderzoek
                            </div>
                            <div className={classes.icon}>
                              $
                            </div>
                          </div>
                        </div>
                        <div className={classes.btn}>
                          <div className={classes.inner}>
                            <div className={classes.label}>
                              Deel studie
                            </div>
                            <div className={classes.icon}>
                              $
                            </div>
                          </div>
                        </div>
                        <div className={classes.btn}>
                          <div className={classes.inner}>
                            <div className={classes.label}>
                              Voeg toe aan overzicht
                            </div>
                            <div className={classes.icon}>
                              $
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className={classes.details}>
                      <hr className={classes.overviewhr} />
                      <p className={classes.desc}>{entry.detail.brief_summary}</p>
                      <h4>Leeftijd</h4>
                      <p>{entry.detail.minimum_age}</p>
                      <h4>Fase Onderzoek</h4>
                      <p>{entry.detail.last_known_status}</p>
                      <h4>Bij diagnose</h4>
                      <p>{entry.detail.condition}</p>
                      <div className={classes.studybtns}>
                        <span>Bekijk de details</span>

                      </div>
                    </div>
                    <p></p>
                  </div>
                );
              })
            }
          </div>
          <div className={classes.overviewbtn}>
            <button>Mijn overzicht</button>
            <button>Deel overzicht</button>
          </div>
        </div>
      </div>
      <Footer
      />
      </div>
    );
  }
}

export default Home;
