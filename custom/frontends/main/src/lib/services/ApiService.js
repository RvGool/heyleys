// @ts-nocheck
import { getClient } from '../helpers/getClient';
import * as ecc from 'eosjs-ecc';
import * as ecies from 'standard-ecies';
const crypto = require('crypto');

class ApiService {
  static encrypt(message) {
    let pubKey = ecc.privateToPublic(localStorage.getItem('user_key'));
    const pubBuffer = ecc.PublicKey(pubKey).toUncompressed().toBuffer();
    const messageBuffer = Buffer.from(message, 'utf8');
    const encryptedBuffer = ecies.encrypt(pubBuffer, messageBuffer);
    return encryptedBuffer;
  }
  
  static decrypt(encryptArr) {
    if(!encryptArr) return [];
    let decryptArr = [];
    for(let i = 0; i < encryptArr.length; i++) {
      const wif = localStorage.getItem('user_key');
      const ecdh = crypto.createECDH('secp256k1');
      const privBuffer = ecc.PrivateKey(wif).toBuffer();
      ecdh.setPrivateKey(privBuffer);
      let encryptBuffer = Buffer.from(encryptArr[i].toLowerCase(),'hex');
      decryptArr.push(ecies.decrypt(ecdh, encryptBuffer).toString());
    }
    return decryptArr;
  }

  static async getStudies() {
    const {rpc, api} = await getClient("eosjs");
    const res = await rpc.get_table_rows({
      json: true,               // Get the response as json
      code: 'heyleysstud2',      // Contract that we target
      scope: 'heyleysstud2',         // Account that owns the data
      table: 'details',        // Table name
      limit: 30,                // Maximum number of rows that we want to get
      reverse: false,           // Optional: Get reversed data
      show_payer: false          // Optional: Show ram payer
    });
    return res.rows;
  }
}

export default ApiService;
