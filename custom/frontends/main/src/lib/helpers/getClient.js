
import { createClient } from '@liquidapps/dapp-client';
import { Api, JsonRpc } from 'eosjs';
import { JsSignatureProvider } from 'eosjs/dist/eosjs-jssig';

export const getClient = async(type, network, endpoint) => {
    if(type == "dapp-client") {
        const client_network = network || "jungle";
        const client_endpoint = process.env.REACT_APP_EOS_HTTP_ENDPOINT || endpoint;
        return await createClient({ network: client_network, httpEndpoint: client_endpoint, fetch: window.fetch.bind(window) });
    } else if (type == "eosjs") {
        const privateKeys = [process.env.REACT_APP_EOS_STUDYDATA_KEY];

        const signatureProvider = new JsSignatureProvider(privateKeys);
        const rpc = new JsonRpc(process.env.REACT_APP_EOS_HTTP_ENDPOINT); //required to read blockchain state
        const api = new Api({ rpc, signatureProvider }); //required to submit transactions

        return await {rpc, api}
    }
};