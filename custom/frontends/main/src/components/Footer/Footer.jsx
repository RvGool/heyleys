import React from 'react';
import classes from './Footer.module.scss';
import Github from 'assets/Footer/github.svg';
import Twitter from 'assets/Footer/twitter.svg';
import LinkedIn from 'assets/Footer/linkedIn.svg';
import Medium from 'assets/Footer/medium.svg';
import Telegram from 'assets/Footer/telegram.svg';

const footer = () => {
  return (
    <footer>
      <div className={classes.container}>
        <div className={classes.flexColumn}>
          <div className={classes.flexSubColumn}>
            <div className={classes.columnTitleText}>Heyleys</div>
            <div className={classes.columnTextDiv}><a className={classes.columnText} href="#" rel="noopener noreferrer" target="_blank"># klinische onderzoeken</a></div>
            <div className={classes.columnTextDiv}><a className={classes.columnText} href="#" rel="noopener noreferrer" target="_blank">Over klinische onderzoeken</a></div>
            <div className={classes.columnTextDiv}><a className={classes.columnText} href="#" rel="noopener noreferrer" target="_blank">Woordenlijst</a></div>
          </div>
          <div className={classes.flexSubColumn}>
            <div className={classes.columnTitleText}>Meer info</div>
            <div className={classes.columnTextDiv}><a className={classes.columnText} href="#" rel="noopener noreferrer" target="_blank">Over Heyleys</a></div>
            <div className={classes.columnTextDiv}><a className={classes.columnText} href="#" rel="noopener noreferrer" target="_blank">Cookies</a></div>
            <div className={classes.columnTextDiv}><a className={classes.columnText} href="#" rel="noopener noreferrer" target="_blank">Algemene voorwaarden</a></div>
          </div>
          <div className={classes.flexSubColumn}>
            <div className={classes.columnTitleText}>Contact</div>
            <div className={classes.columnTextDiv}><a className={classes.columnText} href="#" rel="noopener noreferrer" target="_blank">info@heyleys.nl</a></div>
          </div>
          <div className={classes.flexSubColumn}>
            <div className={classes.columnTitleText}>&nbsp;</div>
            <div className={classes.columnTextDiv}></div>
            <div className={classes.columnTextDiv}><div className={classes.columnText}>Met vriendelijke groet,</div></div>
            <img src="" alt="" />
          </div>
        </div>
        <hr className={classes.footerHr}></hr>
        <div className={classes.copyright}>© 2021 Powered by: Iclusion</div>
      </div>
    </footer>
  );
};

export default footer;
