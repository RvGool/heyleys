import React from 'react';
import classes from './Header.module.scss';
import Button from 'components/UI/Button/Button';
import Logo from 'assets/Logo/heyleys-nederland.png';
import Login from 'components/UI/Login/Login';

const header = (props) => {
  return (
    <nav className={classes.menu}>
      <div className={classes.container}>
        <div className={classes.subcontainer}>
          <a href="#"># klinische onderzoeken</a>
          <a href="#">Over Heyleys</a>
        </div>
        <div className={classes.middle}>
          <div><img className={classes.logo} src={Logo} alt="LiquidApps Logo"/></div>
        </div>
        <div className={classes.subcontainer}>
          <a href="#">Over klinische onderzoeken</a>
          <a href="#">Handige tips</a>
        </div>
      </div>
    </nav>
  );
};

export default header;
