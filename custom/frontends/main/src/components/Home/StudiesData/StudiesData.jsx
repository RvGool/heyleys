import React from 'react';
import classes from './StudiesData.module.scss';

const studiesData = (props) => {

  console.log(props)

  return (
    <div className={classes.container}>
      <div className="stats">
        {/* <h2>{studies.length} Klinische onderzoeken gevonden </h2> */}
        <div className={classes.studycontainer}>
          <div className={classes.studyheader}>
            <h2>/title/</h2>
            <div className={classes.studybtns}>
              <div className={classes.btn}>
                <div className={classes.inner}>
                  <div className={classes.label}>
                    Print onderzoek
                  </div>
                  <div className={classes.icon}>
                    $
                  </div>
                </div>
              </div>
              <div className={classes.btn}>
                <div className={classes.inner}>
                  <div className={classes.label}>
                    Deel studie
                  </div>
                  <div className={classes.icon}>
                    $
                  </div>
                </div>
              </div>
              <div className={classes.btn}>
                <div className={classes.inner}>
                  <div className={classes.label}>
                    Voeg toe aan overzicht
                  </div>
                  <div className={classes.icon}>
                    $
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className={classes.details}>
            <hr className={classes.overviewhr} />
            <p className={classes.desc}>lorem impsum</p>
            <h4>Leeftijd</h4>
            <p></p>
            <h4>Fase Onderzoek</h4>
            <p></p>
            <h4>Bij diagnose</h4>
            <p></p>
            <div className={classes.studybtns}>
              <span>Bekijk de details</span>

            </div>
          </div>
          <p></p>
        </div>
      </div>
      <div className={classes.overviewbtn}>
        <button>Mijn overzicht</button>
        <button>Deel overzicht</button>
      </div>
    </div>
  );
};

export default studiesData;
