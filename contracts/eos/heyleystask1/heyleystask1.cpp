#include "heyleystask1.hpp"

CONTRACT_START()

string api_call(string urlstring)
{
  std::vector<char> data(urlstring.begin(), urlstring.end());
  
  auto dataset = getURI(data, [&]( auto& results ) {
    eosio::check(results.size() > 0, "no results returned");
    auto itr = results.begin();
    return itr->result;
  });

  string datastring(dataset.begin(), dataset.end());
  return datastring;
}

string obtain_studyfields_from_datastring(string datastring)
{
  std::size_t pos = datastring.find("\"StudyFields\":");
  string fields = datastring.substr(pos+15);
  return fields;
}

std::vector<string> obtain_NCTId_of_studies(std::vector<string> arrayofstudies, string fields)
{
  std::size_t nctidpos = fields.find("\"NCTId\":[");
  std::size_t nctilength = 11;
  string nctid = fields.substr(nctidpos+11, nctilength);

  arrayofstudies.push_back(nctid);

  std::size_t endpos = fields.find("}");
  fields = fields.substr(endpos+2);

  if (fields.find("\"NCTId\":[") == std::string::npos)
  {
    return arrayofstudies;
  }

  return obtain_NCTId_of_studies(arrayofstudies, fields);
}

void retrieve_study(string nctid)
{
  action(
    permission_level{get_self(),"active"_n},
    name("heyleysstud2"),
    name("getstudydet"),
    std::make_tuple(get_self(), nctid)
  ).send();

  action(
    permission_level{get_self(),"active"_n},
    name("heyleyscont1"),
    name("getcontdet"),
    std::make_tuple(get_self(), nctid)
  ).send();

  action(
    permission_level{get_self(),"active"_n},
    name("heyleysoutc1"),
    name("getoutcdet"),
    std::make_tuple(get_self(), nctid)
  ).send();
}

[[eosio::action]] void schedule(int32_t seconds, int32_t items) {
  CronDetails cronDetails;
  cronDetails.itemCount = items;
  std::vector<char> rawPayload = eosio::pack<CronDetails>(cronDetails);
  /* Schedule timer with one minute in between */
  schedule_timer(_self, rawPayload, seconds);
}

// remove timer by scope
[[eosio::action]] void removetimer(name account) {
  remove_timer(account);
}

[[eosio::action]] void testharmony() {
  string urlstring = "https://clinicaltrials.gov/api/query/study_fields?expr=AREA[ResultsFirstPostDate]MISSING+AND+AREA[StudyFirstPostDate]RANGE[01%2F01%2F2015%2C+MAX]+AND+cancer&fields=NCTId&max_rnk=1&fmt=json";
  string datastring = api_call(urlstring);
  string fields = obtain_studyfields_from_datastring(datastring);

  std::string::iterator end_pos = std::remove(fields.begin(), fields.end(), ' ');
  fields.erase(end_pos, fields.end());

  std::vector<string> emptyarray;
  auto arrayofstudies = obtain_NCTId_of_studies(emptyarray, fields);

  for(uint32_t i = 0; i < arrayofstudies.size(); i++)
  {
    retrieve_study(arrayofstudies[i]);
  }
}

bool timer_callback(name timer, std::vector<char> payload, uint32_t seconds) {
  auto pl = eosio::unpack<CronDetails>(payload);
  string urlstring = "https://clinicaltrials.gov/api/query/study_fields?expr=AREA[ResultsFirstPostDate]MISSING+AND+AREA[StudyFirstPostDate]RANGE[01%2F01%2F2015%2C+MAX]+AND+cancer&fields=NCTId&max_rnk="+to_string(pl.itemCount)+"&fmt=json";
  string datastring = api_call(urlstring);
  string fields = obtain_studyfields_from_datastring(datastring);

  std::string::iterator end_pos = std::remove(fields.begin(), fields.end(), ' ');
  fields.erase(end_pos, fields.end());

  std::vector<string> emptyarray;
  auto arrayofstudies = obtain_NCTId_of_studies(emptyarray, fields);

  for(uint32_t i = 0; i < arrayofstudies.size(); i++)
  {
    retrieve_study(arrayofstudies[i]);
  }

  return true;
}

CONTRACT_END((schedule)(removetimer)(testharmony))

