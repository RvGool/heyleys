#pragma once
#include <eosio/eosio.hpp>
#include <eosio/system.hpp>

#include "../dappservices/cron.hpp"
#include "../dappservices/oracle.hpp"
#include "../dappservices/multi_index.hpp"
#include <unordered_map>

#define DAPPSERVICES_ACTIONS() \
  XSIGNAL_DAPPSERVICE_ACTION \
  ORACLE_DAPPSERVICE_ACTIONS \
  CRON_DAPPSERVICE_ACTIONS
#define DAPPSERVICE_ACTIONS_COMMANDS() \
  ORACLE_SVC_COMMANDS() \
  CRON_SVC_COMMANDS()

#define CONTRACT_NAME() heyleystask1

using std::string;
using namespace std;
using namespace eosio;

struct CronDetails
{
  uint32_t itemCount;
};

struct StudyDetails
{
  uint32_t id;
  string nctid;
  string registry_date;
  string primary_sponsor;
  string secondary_sponsors;
  string public_title;
  string scientific_title;
  string country;
  string condition;
  string intervention;
  string eligibility_criteria;
  string study_type;
  string start_date;
  string last_known_status;
  string overall_status;
  string completion_date;
  string acronym;
  string minimum_age;
  string maximum_age;
  string gender;
  string brief_summary;
};

struct StudyContacts
{
  uint32_t id;
  string nctid;
  string central_contact_email;
  string central_contact_name;
  string central_contact_phone;
  string central_contact_phone_ext;
  string central_contact_role;
  string location_contact_email;
  string location_contact_name;
  string location_contact_phone;
  string location_contact_phone_ext;
  string location_contact_role;
  string point_of_contact_email;
  string point_of_contact_organization;
  string point_of_contact_phone;
  string point_of_contact_phone_ext;
  string point_of_contact_title;
};