#pragma once
#include <eosio/eosio.hpp>
#include <eosio/system.hpp>

#include "../dappservices/oracle.hpp"
#include "../dappservices/multi_index.hpp"
#include <unordered_map>

#define DAPPSERVICES_ACTIONS() \
  XSIGNAL_DAPPSERVICE_ACTION \
  ORACLE_DAPPSERVICE_ACTIONS \
  IPFS_DAPPSERVICE_ACTIONS
#define DAPPSERVICE_ACTIONS_COMMANDS() \
  ORACLE_SVC_COMMANDS() \
  IPFS_SVC_COMMANDS()

#define CONTRACT_NAME() heyleyscont1

using std::string;
using namespace std;
using namespace eosio;

struct StudyContacts
{
  uint32_t id;
  string nctid;
  string central_contact_email;
  string central_contact_name;
  string central_contact_phone;
  string central_contact_phone_ext;
  string central_contact_role;
  string location_contact_email;
  string location_contact_name;
  string location_contact_phone;
  string location_contact_phone_ext;
  string location_contact_role;
  string point_of_contact_email;
  string point_of_contact_organization;
  string point_of_contact_phone;
  string point_of_contact_phone_ext;
  string point_of_contact_title;
  EOSLIB_SERIALIZE(
    StudyContacts, 
    (id)(nctid)
    (central_contact_email)(central_contact_name)(central_contact_phone)(central_contact_phone_ext)(central_contact_role)
    (location_contact_email)(location_contact_name)(location_contact_phone)(location_contact_phone_ext)(location_contact_role)
    (point_of_contact_email)(point_of_contact_organization)(point_of_contact_phone)(point_of_contact_phone_ext)(point_of_contact_title)
  )
};