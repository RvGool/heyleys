#include "heyleyscont1.hpp"

CONTRACT_START()

string api_call(string urlstring)
{
  std::vector<char> data(urlstring.begin(), urlstring.end());
  
  auto dataset = getURI(data, [&]( auto& results ) {
    eosio::check(results.size() > 0, "no results returned");
    auto itr = results.begin();
    return itr->result;
  });

  string datastring(dataset.begin(), dataset.end());
  return datastring;
}

string obtain_studyfields_from_datastring(string datastring)
{
  std::size_t pos = datastring.find("\"StudyFields\":");
  string fields = datastring.substr(pos+15);
  return fields;
}

std::unordered_map<string, string> parse_studyfields_into_object(string fields, string keys[], uint32_t count)
{
  std::unordered_map<string, string> studyfields = {};

  for (uint32_t i = 0; i < count; i++)
  {
    std::size_t keylength = keys[i].length();
    std::size_t keypos = fields.find("\""+keys[i]+"\":[");

    std::string emptycheckstring = fields.substr((keypos+keylength+2));
    std::size_t emptycheckstartpos = emptycheckstring.find("[");
    std::size_t emptycheckendpos = emptycheckstring.find("]");

    if ((emptycheckendpos - emptycheckstartpos) <= 1) {
      string key = keys[i];
      studyfields[key] = "NULL";
    } else {
      std::size_t valuepos = fields.find("\"", (keypos+keylength+2));
      std::size_t valuelength = fields.find("\"", (valuepos+2));
      std::size_t commacheck = fields.find(",", valuelength);
      
      if((commacheck - valuelength) <= 1)
      {
        string value = emptycheckstring.substr((emptycheckstartpos+1), (emptycheckendpos-2));
        string key = keys[i];
        studyfields[key] = value;
      } else {
        string value = fields.substr(valuepos+1, (valuelength - valuepos) -1);
        string key = keys[i];
        studyfields[key] = value;
      }
    }
  }

  return studyfields;
}

[[eosio::action]] void getcontdet(name user, string nctid) {
  if (user==name("heyleystask1"))
  {
    require_auth(user);
  } else if (user==name("heyleystest1")) {
    require_auth(user);
  } else {
    require_auth(_self);
  }

  string urlstring = "https://clinicaltrials.gov/api/query/study_fields?expr="+nctid+"&fields=CentralContactEMail%2CCentralContactName%2CCentralContactPhone%2CCentralContactPhoneExt%2CCentralContactRole%2CLocationContactEMail%2CLocationContactName%2CLocationContactPhone%2CLocationContactPhoneExt%2CLocationContactRole%2CPointOfContactEMail%2CPointOfContactOrganization%2CPointOfContactPhone%2CPointOfContactPhoneExt%2CPointOfContactTitle&min_rnk=1&fmt=json";
  string datastring = api_call(urlstring);
  string fields = obtain_studyfields_from_datastring(datastring);
  string keys[15] = {"CentralContactEMail", "CentralContactName", "CentralContactPhone", "CentralContactPhoneExt", "CentralContactRole", "LocationContactEMail", "LocationContactName", "LocationContactPhone", "LocationContactPhoneExt", "LocationContactRole", "PointOfContactEMail", "PointOfContactOrganization", "PointOfContactPhone", "PointOfContactPhoneExt", "PointOfContactTitle"};
  std::unordered_map<string, string> contactdetails = parse_studyfields_into_object(fields, keys, 15);
  
  struct StudyContacts contacts;
  int32_t id = std::stoull(nctid.substr(3));

  contacts.id = id;
  contacts.nctid = nctid;
  contacts.central_contact_email = contactdetails["CentralContactEMail"];
  contacts.central_contact_name = contactdetails["CentralContactName"];
  contacts.central_contact_phone = contactdetails["CentralContactPhone"];
  contacts.central_contact_phone_ext = contactdetails["CentralContactPhoneExt"];
  contacts.central_contact_role = contactdetails["CentralContactRole"];
  contacts.location_contact_email = contactdetails["LocationContactEMail"];
  contacts.location_contact_name = contactdetails["LocationContactName"];
  contacts.location_contact_phone = contactdetails["LocationContactPhone"];
  contacts.location_contact_phone_ext = contactdetails["LocationContactPhoneExt"];
  contacts.location_contact_role = contactdetails["LocationContactRole"];
  contacts.point_of_contact_email = contactdetails["PointOfContactEMail"];
  contacts.point_of_contact_organization = contactdetails["PointOfContactOrganization"];
  contacts.point_of_contact_phone = contactdetails["PointOfContactPhone"];
  contacts.point_of_contact_phone_ext = contactdetails["PointOfContactPhoneExt"];
  contacts.point_of_contact_title = contactdetails["PointOfContactTitle"];

  upsert(_self, contacts);
}

[[eosio::action]] void upsertmult(name user, std::vector<StudyContacts> contacts) {
  if (user==name("heyleystask1"))
  {
    require_auth(user);
  } else if (user==name("heyleystest1")) {
    require_auth(user);
  } else {
    require_auth(_self);
  }

  for (StudyContacts contact : contacts)
  {
    upsert(user, contact);
  }
}

[[eosio::action]] void upsert(name user, StudyContacts contact) {
  if (user==name("heyleystask1"))
  {
    require_auth(user);
  } else if (user==name("heyleystest1")) {
    require_auth(user);
  } else {
    require_auth(_self);
  }

  contacts_def contactstable(get_self(), _self.value);
  auto iterator = contactstable.find(contact.id);
  if( iterator == contactstable.end() )
  {
    contactstable.emplace(get_self(), [&]( auto& row ) {
      row.contact = contact;
    });
  }
  else {
    contactstable.modify(iterator, get_self(), [&]( auto& row ) {
      row.contact = contact;
    });
  }
}

private:
  TABLE contacts {
    StudyContacts contact;
    auto primary_key() const { return contact.id; }
  };
  typedef eosio::multi_index<name("contacts"), contacts> contacts_def;

CONTRACT_END((upsertmult)(upsert)(getcontdet))

