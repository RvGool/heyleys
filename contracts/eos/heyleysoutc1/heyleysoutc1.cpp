#include "heyleysoutc1.hpp"

CONTRACT_START()

string api_call(string urlstring)
{
  std::vector<char> data(urlstring.begin(), urlstring.end());
  
  auto dataset = getURI(data, [&]( auto& results ) {
    eosio::check(results.size() > 0, "no results returned");
    auto itr = results.begin();
    return itr->result;
  });

  string datastring(dataset.begin(), dataset.end());
  return datastring;
}

string obtain_studyfields_from_datastring(string datastring)
{
  std::size_t pos = datastring.find("\"StudyFields\":");
  string fields = datastring.substr(pos+15);
  return fields;
}

std::unordered_map<string, string> parse_studyfields_into_object(string fields, string keys[], uint32_t count)
{
  std::unordered_map<string, string> studyfields = {};

  for (uint32_t i = 0; i < count; i++)
  {
    std::size_t keylength = keys[i].length();
    std::size_t keypos = fields.find("\""+keys[i]+"\":[");

    std::string emptycheckstring = fields.substr((keypos+keylength+2));
    std::size_t emptycheckstartpos = emptycheckstring.find("[");
    std::size_t emptycheckendpos = emptycheckstring.find("]");

    if ((emptycheckendpos - emptycheckstartpos) <= 1) {
      string key = keys[i];
      studyfields[key] = "NULL";
    } else {
      std::size_t valuepos = fields.find("\"", (keypos+keylength+2));
      std::size_t valuelength = fields.find("\"", (valuepos+2));
      std::size_t commacheck = fields.find(",", valuelength);
      
      if((commacheck - valuelength) <= 1)
      {
        string value = emptycheckstring.substr((emptycheckstartpos+1), (emptycheckendpos-2));
        string key = keys[i];
        studyfields[key] = value;
      } else {
        string value = fields.substr(valuepos+1, (valuelength - valuepos) -1);
        string key = keys[i];
        studyfields[key] = value;
      }
    }
  }

  return studyfields;
}

[[eosio::action]] void getoutcdet(name user, string nctid)
{
  if (user==name("heyleystask1"))
  {
    require_auth(user);
  } else if (user==name("heyleystest1")) {
    require_auth(user);
  } else {
    require_auth(_self);
  }

  string urlstring = "https://clinicaltrials.gov/api/query/study_fields?expr="+nctid+"&fields=PrimaryOutcomeDescription%2CPrimaryOutcomeMeasure%2CPrimaryOutcomeTimeFrame%2CSecondaryOutcomeDescription%2CSecondaryOutcomeMeasure%2CSecondaryOutcomeTimeFrame&min_rnk=1&fmt=json";
  string datastring = api_call(urlstring);
  string fields = obtain_studyfields_from_datastring(datastring);
  string keys[6] = {"PrimaryOutcomeDescription", "PrimaryOutcomeMeasure", "PrimaryOutcomeTimeFrame", "SecondaryOutcomeDescription", "SecondaryOutcomeMeasure", "SecondaryOutcomeTimeFrame"};
  std::unordered_map<string, string> outcomesdetails = parse_studyfields_into_object(fields, keys, 6);
  
  struct StudyOutcomes outcomes;
  int32_t id = std::stoull(nctid.substr(3));

  outcomes.id = id;
  outcomes.nctid = nctid;
  outcomes.primary_outcome_description = outcomesdetails["PrimaryOutcomeDescription"];
  outcomes.primary_outcome_measure = outcomesdetails["PrimaryOutcomeMeasure"];
  outcomes.primary_outcome_timeframe = outcomesdetails["PrimaryOutcomeTimeFrame"];
  outcomes.secondary_outcome_description = outcomesdetails["SecondaryOutcomeDescription"];
  outcomes.secondary_outcome_measure = outcomesdetails["SecondaryOutcomeMeasure"];
  outcomes.secondary_outcome_timeframe = outcomesdetails["SecondaryOutcomeTimeFrame"];

  upsert(_self, outcomes);
}

[[eosio::action]] void upsertmult(name user, std::vector<StudyOutcomes> outcomes) {
  if (user==name("heyleystask1"))
  {
    require_auth(user);
  } else if (user==name("heyleystest1")) {
    require_auth(user);
  } else {
    require_auth(_self);
  }

  for (StudyOutcomes outcome : outcomes)
  {
    upsert(user, outcome);
  }
}

[[eosio::action]] void upsert(name user, StudyOutcomes outcome) {
  if (user==name("heyleystask1"))
  {
    require_auth(user);
  } else if (user==name("heyleystest1")) {
    require_auth(user);
  } else {
    require_auth(_self);
  }

  outcomes_def outcomestable(get_self(), _self.value);
  auto iterator = outcomestable.find(outcome.id);
  if( iterator == outcomestable.end() )
  {
    outcomestable.emplace(get_self(), [&]( auto& row ) {
      row.outcome = outcome;
    });
  }
  else {
    outcomestable.modify(iterator, get_self(), [&]( auto& row ) {
      row.outcome = outcome;
    });
  }
}

private:
  TABLE outcomes {
    StudyOutcomes outcome;
    auto primary_key() const { return outcome.id; }
  };
  typedef eosio::multi_index<name("outcomes"), outcomes> outcomes_def;

CONTRACT_END((upsertmult)(upsert)(getoutcdet))

