#pragma once
#include <eosio/eosio.hpp>
#include <eosio/system.hpp>

#include "../dappservices/oracle.hpp"
#include "../dappservices/multi_index.hpp"
#include <unordered_map>

#define DAPPSERVICES_ACTIONS() \
  XSIGNAL_DAPPSERVICE_ACTION \
  ORACLE_DAPPSERVICE_ACTIONS \
  IPFS_DAPPSERVICE_ACTIONS
#define DAPPSERVICE_ACTIONS_COMMANDS() \
  ORACLE_SVC_COMMANDS() \
  IPFS_SVC_COMMANDS()

#define CONTRACT_NAME() heyleysoutc1

using std::string;
using namespace std;
using namespace eosio;

struct StudyOutcomes
{
  uint32_t id;
  string nctid;
  string primary_outcome_description;
  string primary_outcome_measure;
  string primary_outcome_timeframe;
  string secondary_outcome_description;
  string secondary_outcome_measure;
  string secondary_outcome_timeframe;
  EOSLIB_SERIALIZE(
    StudyOutcomes, 
    (id)(nctid)(primary_outcome_description)(primary_outcome_measure)(primary_outcome_timeframe)(secondary_outcome_description)(secondary_outcome_measure)(secondary_outcome_timeframe)
  )
};