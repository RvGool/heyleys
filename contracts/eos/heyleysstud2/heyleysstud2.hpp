#pragma once
#include <eosio/eosio.hpp>
#include <eosio/system.hpp>

#include "../dappservices/oracle.hpp"
#include "../dappservices/multi_index.hpp"
#include <unordered_map>

#define DAPPSERVICES_ACTIONS() \
  XSIGNAL_DAPPSERVICE_ACTION \
  IPFS_DAPPSERVICE_ACTIONS \
  ORACLE_DAPPSERVICE_ACTIONS
#define DAPPSERVICE_ACTIONS_COMMANDS() \
  ORACLE_SVC_COMMANDS() \
  IPFS_SVC_COMMANDS()

#define CONTRACT_NAME() heyleysstud2

using std::string;
using namespace std;
using namespace eosio;

struct StudyDetails
{
  uint32_t id;
  string nctid;
  string registry_date;
  string primary_sponsor;
  string secondary_sponsors;
  string public_title;
  string scientific_title;
  string country;
  string condition;
  string intervention;
  string eligibility_criteria;
  string study_type;
  string start_date;
  string last_known_status;
  string overall_status;
  string completion_date;
  string acronym;
  string minimum_age;
  string maximum_age;
  string gender;
  string brief_summary;
  EOSLIB_SERIALIZE(
    StudyDetails, 
    (id)(nctid)(registry_date)(primary_sponsor)(secondary_sponsors)
    (public_title)(scientific_title)(country)(condition)(intervention)(eligibility_criteria)(study_type)(start_date)(last_known_status)(overall_status)
    (completion_date)(acronym)(minimum_age)(maximum_age)(gender)(brief_summary)
  )
};