#include "heyleysstud2.hpp"

CONTRACT_START()

string api_call(string urlstring)
{
  std::vector<char> data(urlstring.begin(), urlstring.end());
  
  auto dataset = getURI(data, [&]( auto& results ) {
    eosio::check(results.size() > 0, "no results returned");
    auto itr = results.begin();
    return itr->result;
  });

  string datastring(dataset.begin(), dataset.end());
  return datastring;
}

string obtain_studyfields_from_datastring(string datastring)
{
  std::size_t pos = datastring.find("\"StudyFields\":");
  string fields = datastring.substr(pos+15);
  return fields;
}

std::unordered_map<string, string> parse_studyfields_into_object(string fields, string keys[], uint32_t count)
{
  std::unordered_map<string, string> studyfields = {};

  for (uint32_t i = 0; i < count; i++)
  {
    std::size_t keylength = keys[i].length();
    std::size_t keypos = fields.find("\""+keys[i]+"\":[");

    std::string emptycheckstring = fields.substr((keypos+keylength+2));
    std::size_t emptycheckstartpos = emptycheckstring.find("[");
    std::size_t emptycheckendpos = emptycheckstring.find("]");

    if ((emptycheckendpos - emptycheckstartpos) <= 1) {
      string key = keys[i];
      studyfields[key] = "NULL";
    } else {
      std::size_t valuepos = fields.find("\"", (keypos+keylength+2));
      std::size_t valuelength = fields.find("\"", (valuepos+2));
      std::size_t commacheck = fields.find(",", valuelength);
      
      if((commacheck - valuelength) <= 1)
      {
        string value = emptycheckstring.substr((emptycheckstartpos+1), (emptycheckendpos-2));
        string key = keys[i];
        studyfields[key] = value;
      } else {
        string value = fields.substr(valuepos+1, (valuelength - valuepos) -1);
        string key = keys[i];
        studyfields[key] = value;
      }
    }
  }

  return studyfields;
}

[[eosio::action]] void getstudydet(name user, string nctid)
{
  if (user==name("heyleystask1"))
  {
    require_auth(user);
  } else if (user==name("heyleystest1")) {
    require_auth(user);
  } else {
    require_auth(_self);
  }

  string urlstring = "https://clinicaltrials.gov/api/query/study_fields?expr="+nctid+"&fields=StudyFirstPostDate%2CLeadSponsorName%2CCollaboratorName%2CBriefTitle%2COfficialTitle%2CLocationCountry%2CCondition%2CInterventionName%2CEligibilityCriteria%2CStudyType%2CStartDate%2CLastKnownStatus%2COverallStatus%2CCompletionDate%2CAcronym%2CMinimumAge%2CMaximumAge%2CGender%2CBriefSummary&min_rnk=1&fmt=json";
  string datastring = api_call(urlstring);
  string fields = obtain_studyfields_from_datastring(datastring);
  string keys[19] = {"StudyFirstPostDate", "LeadSponsorName", "CollaboratorName", "BriefTitle", "OfficialTitle", "LocationCountry", "Condition", "InterventionName", "EligibilityCriteria", "StudyType", "StartDate", "LastKnownStatus", "OverallStatus", "CompletionDate", "Acronym", "MinimumAge", "MaximumAge", "Gender", "BriefSummary"};
  std::unordered_map<string, string> studydetails = parse_studyfields_into_object(fields, keys, 19);
  
  struct StudyDetails details;
  int32_t id = std::stoull(nctid.substr(3));

  details.id = id;
  details.nctid = nctid;
  details.registry_date = studydetails["StudyFirstPostDate"];
  details.primary_sponsor = studydetails["LeadSponsorName"];
  details.secondary_sponsors = studydetails["CollaboratorName"];
  details.public_title = studydetails["BriefTitle"];
  details.scientific_title = studydetails["OfficialTitle"];
  details.country = studydetails["LocationCountry"];
  details.condition = studydetails["Condition"];
  details.intervention = studydetails["InterventionName"];
  details.eligibility_criteria = studydetails["EligibilityCriteria"];
  details.study_type = studydetails["StudyType"];
  details.start_date = studydetails["StartDate"];
  details.last_known_status = studydetails["LastKnownStatus"];
  details.overall_status = studydetails["OverallStatus"];
  details.completion_date = studydetails["CompletionDate"];
  details.acronym = studydetails["Acronym"];
  details.minimum_age = studydetails["MinimumAge"];
  details.maximum_age = studydetails["MaximumAge"];
  details.gender = studydetails["Gender"];
  details.brief_summary = studydetails["BriefSummary"];

  upsert(_self, details);
}

[[eosio::action]] void upsertmult(name user, std::vector<StudyDetails> studies) {
  if (user==name("heyleystask1"))
  {
    require_auth(user);
  } else if (user==name("heyleystest1")) {
    require_auth(user);
  } else {
    require_auth(_self);
  }

  for (StudyDetails study : studies)
  {
    upsert(user, study);
  }
}

[[eosio::action]] void upsert(name user, StudyDetails detail) {
  if (user==name("heyleystask1"))
  {
    require_auth(user);
  } else if (user==name("heyleystest1")) {
    require_auth(user);
  } else {
    require_auth(_self);
  }

  details_def detailstable(get_self(), _self.value);
  auto iterator = detailstable.find(detail.id);
  if( iterator == detailstable.end() )
  {
    detailstable.emplace(get_self(), [&]( auto& row ) {
      row.detail = detail;
    });
  }
  else {
    detailstable.modify(iterator, get_self(), [&]( auto& row ) {
      row.detail = detail;
    });
  }
}

private:
  TABLE details {
    StudyDetails detail;
    auto primary_key() const { return detail.id; }
  };
  typedef eosio::multi_index<name("details"), details> details_def;

CONTRACT_END((upsertmult)(upsert)(getstudydet))

